#!/bin/bash

COMPOSEBIN=${COMPOSEBIN-"docker-compose"}

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

function performStart
{
  $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml up
}

function performStop
{
  $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml stop
}

function showOpsHelp
{
  cat <<EOF
Usage: ops package outputfile
Usage: ops restore
EOF
}

function showHelp
{
  cat <<EOF
Usage: start
Usage: stop
EOF
  showOpsHelp
}

function performOps
{
  case "$1" in
    package)
      [ -z "$2" ] && echo "Missing output file argument" && exit 1;
      [ -e "$2" ] && [ ! -w "$2" ] && echo "Target file is not writeable" && exit 1;
      $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml stop crowd
      $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml -f $SCRIPT_DIR/docker-compose.ops.yml run --rm crowdops package /var/ops/$2
      $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml start crowd
      ;;
    restore)
      [ -z "$2" ] && echo "Missing output file argument" && exit 1;
      [ -e "$2" ] && [ ! -w "$2" ] && echo "Target file is not writeable" && exit 1;
      $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml stop crowd
      $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml -f $SCRIPT_DIR/docker-compose.ops.yml run --rm crowdops restore /var/ops/$2
      $COMPOSEBIN -f $SCRIPT_DIR/docker-compose.yml start crowd
      ;;
    *)
      showOpsHelp
      ;;
  esac
}

function main
{
  case "$1" in
    start)
      performStart
      ;;
    stop)
      performStop
      ;;
    ops)
      shift
      performOps "$@"
      ;;
    *)
      showHelp
      ;;
  esac
}

main "$@"